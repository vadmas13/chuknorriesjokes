import * as React from 'react';
import Styled from './assets/styles/styles'
import {getJoke} from "./api/getJokes";
import {useEffect, useReducer, useState} from "react";
import {stateJokes} from "./types/types";
import {initialStateLoadData, loadJokeReducer} from "./reducer/reducer";
import {BrowserRouter, Route} from 'react-router-dom'
import Home from "./components/Home/Home";
import ListJokes from "./components/ListJokes/ListJokes";
import HeaderNav from "./components/HeaderNav/HeaderNav";
import {initialStatePopupData, loadPopupReducer} from "./reducer/reducerPopup";
import Popup from "./components/Popup/Popup";


const App = () => {

    const [joke, setJoke] = useState<stateJokes | null>(null);
    const TIMER = 3;
    const storageName = 'jokes';

    const [favouriteJokes, setFavourite] = useState<Array<stateJokes>>(JSON.parse(localStorage.getItem(storageName) as string));
    const [isFavourite, setIsFavourite] = useState<string>('');

    const [stateLoadData, dispatchJoke] = useReducer(loadJokeReducer, initialStateLoadData);
    const [statePopup, dispatchPopup] = useReducer(loadPopupReducer, initialStatePopupData);

    const [jokeTimer, setJokeTimer] = useState<boolean>(false);

    const [timer, setTimer] = useState<number>(0);


    useEffect(() => {
        if (!stateLoadData.initialLoaded) {
            dispatchJoke({type: 'INITIAL_LOADED'});
            if (!stateLoadData.loading) {
                getJoke(setJoke, dispatchJoke);
            }
        }

        if (!stateLoadData.loading) {
            if (jokeTimer && timer === 0) {
                getJokesTimer()
            }
        }

        if (timer !== 0 && !jokeTimer) {
            clearTimeout(timer);
            setTimer(0);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [joke, jokeTimer, stateLoadData.loading]);

    useEffect(() => {
        checkActiveJoke()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [favouriteJokes])


    const getNewJoke = () => {
        if (!stateLoadData.loading) {
            setJoke(null);
            getJoke(setJoke, dispatchJoke);
        }
    }

    const getJokesTimer = () => {
        if (joke && joke.value) {
            setTimer(setTimeout(() => {
                setTimer(0);
                getNewJoke();
            }, TIMER * 1000));

        }
    }

    const setFavouriteJoke = () => {
        let jokesArray = favouriteJokes ? favouriteJokes : [];

        if (joke) {
            jokesArray = [joke, ...jokesArray];
            setIsFavourite(joke.id);
        }

        if (jokesArray.length > 10) {
            jokesArray.pop();
        }

        localStorage.setItem(storageName, JSON.stringify(jokesArray));
        setFavourite(jokesArray);
    }

    const checkActiveJoke = () => {
        if (favouriteJokes.length) {
            if (!favouriteJokes.some(item => item.id === isFavourite)) {
                setIsFavourite('');
            }
        } else {
            setIsFavourite('');
        }
    }

    const removeJokeFromList = async (id: string) => {
        let jokesArray = favouriteJokes;
        jokesArray = jokesArray.filter(item => item.id !== id);

        localStorage.setItem(storageName, JSON.stringify(jokesArray));
        setFavourite(jokesArray);
    }

    const deleteAllJokes = () => {
        localStorage.setItem(storageName, JSON.stringify([]));
        setFavourite([]);
    }


    return (
        <BrowserRouter>
            <Styled.ContainerApp>
                <Styled.Favourite>
                    <HeaderNav length={favouriteJokes.length}/>
                </Styled.Favourite>
                <Route exact path='/' render={() => <Home joke={joke}
                                                          getNewJoke={getNewJoke}
                                                          setFavouriteJoke={setFavouriteJoke}
                                                          isFavourite={isFavourite}
                                                          setJokeTimer={setJokeTimer}
                                                          jokeTimer={jokeTimer}/>}/>
                <Route exact path='/list' render={() => <ListJokes favouriteJokes={favouriteJokes}
                                                                   deleteAllJokes={deleteAllJokes}
                                                                   removeJokeFromList={removeJokeFromList}
                                                                   dispatchPopup={dispatchPopup}/>}/>
            </Styled.ContainerApp>
            {statePopup.modal && <Popup statePopup={statePopup}
                                        dispatchPopup={dispatchPopup}/>}
        </BrowserRouter>

    );
}

export default App;
