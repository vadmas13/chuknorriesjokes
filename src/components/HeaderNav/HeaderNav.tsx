import * as React from 'react';
import {FC} from "react";
import favourite from "../../assets/img/favourite.svg";
import arrow from "../../assets/img/arrow.svg";
import {Link, Route} from "react-router-dom";
import Styled from "../../assets/styles/styles";

type propTypes = {
    length: number
}

const HeaderNav: FC<propTypes> = ({length}) => {

    return (
        <>
            <Route exact path='/'>
                <Styled.CountFavourite>
                    { length }
                </Styled.CountFavourite>
                <Link to="/list">
                    <Styled.Image src={favourite} alt={'img'} />
                </Link>
            </Route>
            <Route exact path='/list' >
                <Link to="/">
                    <Styled.Image src={arrow} alt={'img'} />
                </Link>
            </Route>
        </>
    )
}

export default HeaderNav