import * as React from 'react';
import './popup.css'
import {ActionTypes, initialStatePopupType} from "../../types/reducer";
import {Dispatch, FC} from "react";
import Styled from "../../assets/styles/styles";
import {actionsPopup} from "../../reducer/reducerPopup";

type propTypes = {
    statePopup: initialStatePopupType
    dispatchPopup: Dispatch<ActionTypes<typeof actionsPopup>>
}

const Popup:FC<propTypes> = ({statePopup, dispatchPopup}) => {


    const successPopup = () => {
        statePopup.callback(statePopup.id);
        closePopup();
    }

    const closePopup = () => {
        dispatchPopup({type: 'CLOSE_MODAL'})
    }

    return(
        <div className="modal">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3 className="modal-title">Delete joke?</h3>
                        <span onClick={() => closePopup()} title="Close" className="close">×</span>
                    </div>
                    <div className="modal-body">
                        <p>  {  statePopup.text.length > 100? statePopup.text.slice(0, 100) + '...' : statePopup.text }</p>
                        <Styled.ButtonsBlock>
                            <Styled.Button onClick={() => successPopup()}>
                                Yes
                            </Styled.Button>
                            <Styled.Button onClick={() => closePopup()}>
                                No
                            </Styled.Button>
                        </Styled.ButtonsBlock>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Popup;