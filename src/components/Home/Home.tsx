import * as React from 'react';
import Styled from "../../assets/styles/styles";
import preloaderGif from "../../assets/img/preloader.gif";
import {stateJokes} from "../../types/types";
import {FC} from "react";


type HomePropTypes = {
    joke: stateJokes | null
    getNewJoke: () => void
    setJokeTimer: (a: boolean) => void
    jokeTimer: boolean
    setFavouriteJoke: () => void
    isFavourite: string
}

const Home: FC<HomePropTypes> = ({joke, getNewJoke, setJokeTimer, jokeTimer,
                                     setFavouriteJoke, isFavourite}) => {
    return (
        <Styled.ContainerApp>
            <Styled.TitleHeader>
                Chuck Norris Jokes
            </Styled.TitleHeader>
            {joke ? (
                    <Styled.JokeRender>
                        {joke.value}
                        <Styled.ImageJoke src={joke.icon_url} right={true}/>
                    </Styled.JokeRender>
                ) :
                <Styled.JokeRender>
                    <Styled.ImageJoke src={preloaderGif}/>
                </Styled.JokeRender>
            }

            <Styled.ButtonsBlock>

                <Styled.Button onClick={() => getNewJoke()}>
                    get Joke
                </Styled.Button>
                <Styled.Button onClick={() => setJokeTimer(!jokeTimer)} jokeTimer={jokeTimer}>
                    {!jokeTimer ? 'get jokes timer' : 'stop jokes'}
                </Styled.Button>
                { joke && isFavourite !== joke.id &&  <Styled.Button onClick={() => setFavouriteJoke()}>
                    add to favourite
                </Styled.Button> }
                { joke && isFavourite === joke.id && <Styled.ButtonAdded >
                    added to favourite
                </Styled.ButtonAdded> }
            </Styled.ButtonsBlock>
        </Styled.ContainerApp>
    )
}

export default Home