import * as React from 'react'
import {Dispatch, FC} from "react";
import ListStyled from "../../../assets/styles/list";
import {stateJokes} from "../../../types/types";
import removeSvg from './../../../assets/img/remove.svg';
import {ActionTypes} from "../../../types/reducer";
import {actionsPopup} from "../../../reducer/reducerPopup";

type propTypes = {
    joke: stateJokes
    dispatchPopup: Dispatch<ActionTypes<typeof actionsPopup>>
    removeJokeFromList: (id: string) => void
}

const JokeItem: FC<propTypes> = ({joke, removeJokeFromList, dispatchPopup}) => {

    const removePost = () => {
        dispatchPopup({type: 'OPEN_MODAL', callback: removeJokeFromList, text: joke.value, id: joke.id})
    }

    return(
        <ListStyled.Item>
            { joke.value }
            <ListStyled.DeleteIcon>
                 <img src={removeSvg} alt="removeSvg" onClick={() => removePost()}/>
            </ListStyled.DeleteIcon>
        </ListStyled.Item>
    )
}

export default JokeItem