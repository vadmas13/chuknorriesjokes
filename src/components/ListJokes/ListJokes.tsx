import * as React from 'react';
import Styled from "../../assets/styles/styles";
import ListStyled from "../../assets/styles/list";
import {stateJokes} from "../../types/types";
import {Dispatch, FC} from "react";
import JokeItem from "./JokeItem/JokeItem";
import {ActionTypes} from "../../types/reducer";
import {actionsPopup} from "../../reducer/reducerPopup";

type propTypes = {
    favouriteJokes: Array<stateJokes>
    dispatchPopup: Dispatch<ActionTypes<typeof actionsPopup>>
    removeJokeFromList: (id: string) => void
    deleteAllJokes: () => void
}

const ListJokes: FC<propTypes> = ({favouriteJokes, dispatchPopup, removeJokeFromList, deleteAllJokes}) => {

    const deleteAll = () => {
        dispatchPopup({type: 'OPEN_MODAL', callback: deleteAllJokes, text: 'Delete All Jokes? Really?', id: ''})
    }

    return (
        <Styled.ContainerApp>
            <Styled.TitleHeader>
                Favourite Jokes
            </Styled.TitleHeader>
            <ListStyled.RowList>
                {favouriteJokes && favouriteJokes.length !== 0 ? favouriteJokes.map(item => (
                   <JokeItem joke={item}
                             dispatchPopup={dispatchPopup}
                             removeJokeFromList={removeJokeFromList}
                             key={item.id}/>
                )) : <ListStyled.EmptyTitle>Not have favourite jokes</ListStyled.EmptyTitle> }
            </ListStyled.RowList>
            <Styled.ButtonsBlock>
                { favouriteJokes && favouriteJokes.length !== 0 && <Styled.Button onClick={() => deleteAll()}>
                    delete all jokes
                </Styled.Button>}
            </Styled.ButtonsBlock>
        </Styled.ContainerApp>
    )
}

export default ListJokes