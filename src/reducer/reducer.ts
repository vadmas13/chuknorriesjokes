import {ActionTypes, initialStateLoadDataType} from "../types/reducer";


export const initialStateLoadData: initialStateLoadDataType = {
    loading: false,
    initialLoaded: false
}

export const actionsLoad = {
    INITIAL_LOADED: () => ({type: 'INITIAL_LOADED'} as const),
    LOADING_START: () => ({type: 'LOADING_START'} as const),
    LOADING_END: () => ({type: 'LOADING_END'} as const)
}

export const loadJokeReducer = (state: initialStateLoadDataType, action: ActionTypes<typeof actionsLoad>) => {
    switch (action.type) {
        case "INITIAL_LOADED":
            return { ...state, initialLoaded: true }
        case 'LOADING_END':
            return {...state, loading: false}
        case "LOADING_START":
            return {...state, loading: true}
    }

}

