import { ActionTypes as  ATypes, initialStatePopupType} from "../types/reducer";


export const initialStatePopupData: initialStatePopupType = {
    modal: false,
    text: '',
    callback: () => {},
    id: ''
}

export const actionsPopup = {
    OPEN_MODAL: (callback: (id: string) => void, text: string, id?: string) => ({type: 'OPEN_MODAL', callback, text, id} as const),
    CLOSE_MODAL: () => ({type: 'CLOSE_MODAL'} as const)
}

export const loadPopupReducer = (state: initialStatePopupType = initialStatePopupData, action: ATypes<typeof actionsPopup>) => {
    switch (action.type) {
        case "OPEN_MODAL":
            return {
                ...state, modal: true, callback: action.callback, text: action.text, id: action.id ? action.id : ''
            }
        case "CLOSE_MODAL":
            return {...initialStatePopupData}
    }

}

