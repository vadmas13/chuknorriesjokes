import styled from 'styled-components'

export default {

    RowList: styled.div`
        width: 100%;
        display: flex;
        flex-flow: wrap;
        justify-content: space-between;
    `,

    EmptyTitle: styled.p`
        text-align: center;
        color: #444;
        font-size: 22px;
        width: 100%;
    `,

    Item: styled.div`
        width: 25%;
        min-width: 200px;
        flex-flow: wrap;
        padding: 2%;
        -webkit-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        margin: 20px;
        position: relative;
        padding-top: 40px;
        color: #444;
    `,

    DeleteIcon: styled.div`
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;
        display: flex;
        justify-content: flex-end;
        background: rgba(204,204,204,0.31);
        padding: 5px;
        border-radius: 5px;
        img{
          cursor: pointer;
        }
    `,
}