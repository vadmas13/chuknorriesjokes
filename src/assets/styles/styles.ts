import styled from 'styled-components'

export default {

    ContainerApp: styled.div`
        max-width: 1000px;;
        display: flex;
        margin: 50px auto;
        flex-flow: column;
        align-items: center;
        justify-content: center;
        position: relative;
        
    `,

    Image: styled.img`
        color: red;
        & svg path#star{
          color: red !important;
          fill: red;
        }
    `,

    Favourite: styled.div`
        position: absolute;
        top:0;
        right: 20px;
        width: 48px;
        height: 48px;
    `,

    CountFavourite: styled.div`
        position: absolute;
        top: -14px;
        left: -12px;
        padding: 8px;
        line-height: 7px;
        font-weight: 600;
        font-size: 17px;
        color: rgba(193,0,20,0.79);
        border-radius: 100%;
        background: #fff;
        border: 2px solid rgba(193,0,20,0.79);
    `,


    TitleHeader: styled.h1`
        font-size: 30px;
        color: #4444;
        font-weight: 600;
        text-align: center;
    `,

    ImageJoke: styled.img<{right? : boolean}>`
        display: block;
        margin: 20px auto;
        width: 60px;
        height: 60px;
        margin-right: ${props => props.right ? '20px' : 0};
    `,

    Button: styled.button<{jokeTimer? :boolean}>`
        font-size: 18px;
        color: #fff;
        width: 300px !important;
        background: ${props => props.jokeTimer ? 'rgba(255,130,130,0.79)' : 'rgba(193,0,20,0.79)'} ;
        font-weight: 600;
        text-align: center;
        text-transform: uppercase;
        transition: all .2s;
        border-radius: 5px;
        margin: 5px auto;
        border: 0;
        padding: 15px 20px;
        cursor: pointer;
        &:hover{
         background: ${props => props.jokeTimer ? 'rgba(92,50,50,0.79)' : 'rgba(128,2,15,0.79)'} ;
        }
        &:active, &:focus{
          outline: none;
        }
        &:active{
          background: rgba(27,137,11,0.79);
        }
    `,

    ButtonAdded: styled.button`
        font-size: 18px;
        color: #fff;
        width: 100%;
        background: rgba(27,137,11,0.79);
        font-weight: 600;
        text-align: center;
        text-transform: uppercase;
        transition: all .2s;
        border-radius: 5px;
        margin: 5px auto;
        border: 0;
        padding: 15px 20px;
        cursor: pointer;
        &:active, &:focus{
          outline: none;
        }
       
    `,

    ButtonsBlock: styled.div`
        margin: 0 auto;
        display: flex;
        flex-flow: column;
        
    `,

    JokeRender: styled.div`
        text-align: left;
        color: #1d1d1d;
        font-size: 20px;
        padding: 20px;
        margin: 0 auto;
        display: flex;
        flex-flow: row-reverse;
        max-width: 600px;
        align-items: center;
        height: 300px;
        
    `,
}