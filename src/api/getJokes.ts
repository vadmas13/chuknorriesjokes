import {stateJokes} from "../types/types";
import {Dispatch} from "react";
import {ActionTypes} from "../types/reducer";
import {actionsLoad} from "../reducer/reducer";

export const getJoke =   (callback: (joke: stateJokes) => void, dispatchJoke: Dispatch<ActionTypes<typeof actionsLoad>>): any  => {
        dispatchJoke({type: 'LOADING_START'});
        fetch("https://api.chucknorris.io/jokes/random")
        .then(response => response.json())
        .then(async joke => {
            await callback(joke);
            await dispatchJoke({type: 'LOADING_END'});
        })
        .catch(err => err);
}

