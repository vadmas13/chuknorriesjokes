export type initialStateLoadDataType = {
    loading: boolean
    initialLoaded: boolean
}

export type initialStatePopupType = {
    modal: boolean
    text: string
    callback: (id: string) => void
    id: string
}

export type PropertiesType<T> = T extends {[key: string]: infer U} ?  U : never;
type MyReturnType<T> =  T extends (...args: any[]) => infer R ? R :never; // as ReturnType


export type ActionTypes<T> = MyReturnType<PropertiesType<T>>;
