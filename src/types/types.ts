export type stateJokes = {
    id: string
    value: string
    icon_url: string
    created_at?: string
    url?: string
}